import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import axios from 'axios';
import  Datetime from 'react-datetime';
import Header from "../Header/HeaderComponent.js";
import 'react-datepicker/dist/react-datepicker.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthenticationService from '../../_services/AuthenticationService'
import { DateTimePicker, KeyboardDateTimePicker  } from '@material-ui/pickers'
import { MuiPickersUtilsProvider, InlineDatePicker } from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";
import Icon from "@material-ui/core/Icon";
import Layout from '../Layout'
import './CreateAnnouncements.css'
import QuestionMark from '../../images/question_mark.jpg'
import ParkingIcon from '../../images/parking-car.png'

class CreateAnnouncement extends Component {

  constructor (props) {
    super(props)
    this.state = {
      startDate: moment(),
      endDate:moment(),
      successMsg : '',
      errorMsg : '',
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
   
handleChange(dateName, dateValue) {
    let { startDate, endDate } = this.state;
    if (dateName === 'startDate') {
      startDate = dateValue;
    } else {
      endDate = dateValue;
    }
    this.setState({
      startDate,
      endDate,
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    let startDate = this.state.startDate;
    let endDate = this.state.endDate;
    const dateObj = {
      freeFrom: startDate,
      freeTo:endDate
    }
     AuthenticationService.setupAxiosInterceptors()
    axios.post('http://localhost:8090/announcement/create', dateObj)
        // .then(res => console.log(res.data));

        .then(res => {
            this.setState({
                isLoading : false,
                successMsg : "Обявата е успешно създадена и публикувана!"
            })            
        })
        .catch((error) => {
            this.setState({
                errorMsg : error.response.data.message
            })
        })
  }


  render() {
  
    return (
      <>
      <Header></Header>
      <Layout></Layout>
      <MuiPickersUtilsProvider utils={DateFnsUtils}>
       <div className="create_announcement_body">
        <div className="create_announcement">
          {this.state.successMsg !== '' && 
                    <div className="alert alert-success alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.successMsg}</strong>
                    </div>}
                    {this.state.errorMsg !== '' && 
                    <div className="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.errorMsg}</strong>
                    </div>} 
        <div className ="title">
          <h3>СЪЗДАВАНЕ НА ОБЯВА <img src={ParkingIcon}></img></h3>
        </div>
        <div className = "content">
        <h5>Свободно ли е твоето паркомясто? Може да го споделиш с твоите колеги.</h5>
        <center> <img height='200px' width='250px' src={QuestionMark}></img></center>
         </div>
         <h5>Избери дата, когато твоето паркомясто в сградата ще бъде свободно.</h5>
         
        <form className="date_pickers" onSubmit={ this.handleSubmit }>
            <KeyboardDateTimePicker   
              label="Свободно от:"    
              value={ this.state.startDate }
              onChange={date => this.handleChange('startDate', date)}
              name="startDate"
              autoOk
              variant="inline"
              inputVariant="outlined"
              disablePast
              format="yyyy/MM/dd HH:mm"
              onError={console.log}
               margin="normal"
            />
            <KeyboardDateTimePicker 
              label="Свободно до:"    
              value={ this.state.endDate }
              onChange={date => this.handleChange('endDate', date)}
              name="endDate"
              autoOk
              variant="inline"
              inputVariant="outlined"
              disablePast
              format="yyyy/MM/dd HH:mm"
              onError={console.log}
               margin="normal"
            />
       
        <button type = "submit" className="button"><span>Публикувай</span></button>
           
        </form>
        </div>
        </div>
                 
      </MuiPickersUtilsProvider>
      </>
    );
  }
}

export default CreateAnnouncement;