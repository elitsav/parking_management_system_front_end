import React, {Component} from 'react'

import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import LocalParkingIcon from '@material-ui/icons/LocalParking';
import BusinessIcon from '@material-ui/icons/Business';
import UserService from '../_services/UserService.js'
import AuthenticationService from '../_services/AuthenticationService.js'
import BookImage from '../images/booking_btn.png'
import CreatedAtIcon from '../images/calendar.png'
import FreeFromIcon from '../images/free_from.png'
import FreeToIcon from '../images/free_to.png'
import RemoveUserIcon from '../images/remove-user.png'
import ParkingFloorIcon from '../images/parking_floor.png'
import ParkingSlotIcon from '../images/parking_slot.png'
import UserNameIcon from '../images/name.png'
import MailIcon from '../images/mail.png'
import PhoneIcon from '../images/phone.png'
import { Link } from 'react-router-dom';
import axios from "axios";
import Layout from './Layout';
import Header from './Header/HeaderComponent.js'
import LoadingIndicator from './LoadingIndicator'
import './Announcement/Announcement.css'



class User extends Component {

    constructor(props) {
        super(props)

        this.state = {
            successMsg : '',
            errorMsg : '',
            isLoading : false,
            users : []
        }
        
        this.getAllActiveUsers = this.getAllActiveUsers.bind(this)
        this.deactivateUser=this.deactivateUser.bind(this)
    }

    componentDidMount() {
                 const { match: { params } } = this.props;

        this.getAllActiveUsers();
         axios.get(`http://localhost:8090/user/profile/${params.userUid}`)
    .then(({ data: user }) => {
      console.log('user', user);

      this.setState({ user });
    });
    }

    getAllActiveUsers() {
       AuthenticationService.setupAxiosInterceptors()
        // this.setState({
        //     isLoading : true
        // });

        UserService.getAllActiveUsers()
        .then(response => {
            this.setState({
                isLoading : false,
                users : response.data
            })
        })
    }

   deactivateUser(userUid) {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });
    
        UserService.deactivateUser(userUid)
        .then(response => {
            this.setState({
                isLoading : false,
                successMsg : 'Успешно деактивирахте потребител!'
            })
            this.getAllActiveUsers()
        })
        .catch(() => {
            this.setState({
                errorMsg : `Възникна грешка! Моля опитайте по-късно !`
            })
        })
    }

    render() {
        // if(this.state.isLoading) {
        //    return <LoadingIndicator/>
        // }
        return (
            <>
            <Header></Header>
            <Layout />
            <div className="announcements">
                <div className="bs-example">
                    {this.state.successMsg !== '' && 
                    <div className="alert alert-success alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.successMsg}</strong>
                    </div>}
                    {this.state.errorMsg !== '' && 
                    <div className="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.errorMsg}</strong>
                    </div>} 
                    <h1>Активни потребители</h1>
                    {/* <div styles="overflow-x:auto;"> */}
                    <table className="table table-hover">
                      {/* <caption>Statement Summary</caption> */}
                        <thead>
                            {/* <tr styles="background: ##7B241C;"> */}
                                <th color width='7%'>Email адрес <p /> <img src={MailIcon}></img></th>
                                <th width='7%'>Име<p /><img src={UserNameIcon}></img></th>
                                <th width='7%'>Телефонен номер<p /><img src={PhoneIcon}></img></th>
                                {/* <th width= '14%'>Parking slot
                                    <th  width='7%'>floor<p/> <img src={ParkingFloorIcon}></img></th>
                                    <th  width='7%'>number<p /><img src={ParkingSlotIcon}></img> </th></th> */}
                                <th  width='7%'><mark>Деактивирай потребител</mark></th>
                        </thead>
                        <tbody>
                        {
                            this.state.users.map(
                                user => 
                                  <tr key={user.uid}>
                                    <td data-label="Email адрес">
                                       <Link to={`/admin/user/profile/edit/${user.uid}`}>
                                       {user.email} </Link></td>
                                    <td data-label="Име">
                                       {user.firstName}{' '}{user.lastName} 
                                    </td>
                                     <td data-label="Телефонен номер">
                                       {user.phoneNumber}
                                    </td>
                                    {/* <td data-label = "Parking slot">
                                        <td data-label="floor" >
                                        <img   src={ParkingFloorIcon}></img>
                                        if({user.parkingSlot.floor}){
                                        {user.parkingSlot.floor}}</td>
                                        <td data-label="number">
                                        <img   src={ParkingSlotIcon}></img>
                                        {user.parkingSlot.number}</td>
                                    </td>
                                 */}
                                     <td>
                                         <IconButton>
                                           <img   src={RemoveUserIcon} onClick={() => 
                                              this.deactivateUser(user.uid)}></img>
                                            </IconButton>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                    {/* </div> */}
                </div>
            </div>
            </>
        )
    }
}

export default User;