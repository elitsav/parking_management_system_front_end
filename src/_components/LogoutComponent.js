import React, { Component } from "react";

class LogoutComponent extends Component {
  render() {
    return (
      <>
        <center>
          <mark>
            <b>
              <h1>Излезнохте от сайта успешно!</h1>
            </b>
          </mark>
        </center>

        <form action="/">
          <center>
            <button className="button" onClick="/">
              <span>Обратно към "Вход"</span>
            </button>
          </center>
        </form>
      </>
    );
  }
}

export default LogoutComponent;
