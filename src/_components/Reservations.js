import React, { Component } from "react";

import { IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EmailIcon from "@material-ui/icons/Email";
import ReservationService from "../_services/ReservationService.js";
import AuthenticationService from "../_services/AuthenticationService.js";
import Layout from "./Layout";
import LoadingIndicator from './LoadingIndicator'
import ReservationImage from "../images/reservations.jpg";
import CreatedAtIcon from "../images/calendar.png";
import FreeFromIcon from "../images/free_from.png";
import FreeToIcon from "../images/free_to.png";
import ParkingSlotIcon from "../images/parking_slot.png";
import ParkingFloorIcon from "../images/parking_floor.png";
import ReservationIcon from "../images/reservation_icon.png";
import ReservationTimeIcon from "../images/reservation_time.png";
import UserIcon from "../images/user_icon.png";
import Header from "./Header/HeaderComponent.js";

import "./MyReservations/MyReservations.css";

class Reservations extends Component {
  constructor(props) {
    super(props);

    this.state = {
      successMsg: "",
      errorMsg: "",
      isLoading: false,
      reservations: [],
    };

    this.getAllReservations = this.getAllReservations.bind(
      this
    );
    this.removeUserReservation=this.removeUserReservation.bind(this)
  }

  componentDidMount() {
    this.getAllReservations();
  }

  getAllReservations() {
    AuthenticationService.setupAxiosInterceptors();
    this.setState({
        isLoading : true
    });

    ReservationService.getAllReservations().then((response) => {
      this.setState({
        isLoading: false,
        reservations: response.data,
      });
    });
  }

  removeUserReservation(reservationUid) {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });
    
        ReservationService.removeUserReservation(reservationUid)
        .then(response => {
            this.setState({
                isLoading : false,
                successMsg : response.data.successMsg
            })
            this.getAllReservations()
        })
        .catch(() => {
            this.setState({
                errorMsg : `Възникна грешка! Моля опитайте по-късно ! !`
            })
        })
    }

  render() {
    if(this.state.isLoading) {
        return <LoadingIndicator/>
    }
    return (
      <>
        <Header></Header>
        <Layout />
        <div className="reservations">
          <div className="bs-example">
            <center>
              <img
                className="reservationImage"
                alt=""
                src={ReservationImage}
              ></img>
            </center>
            {this.state.successMsg !== "" && (
              <div
                className="alert alert-success alert-dismissible"
                role="alert"
              >
                <button
                  type="button"
                  className="close"
                  data-dismiss="alert"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong>{this.state.successMsg}</strong>
              </div>
            )}
            {this.state.errorMsg !== "" && (
              <div
                className="alert alert-danger alert-dismissible"
                role="alert"
              >
                <button
                  type="button"
                  className="close"
                  data-dismiss="alert"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong>{this.state.errorMsg}</strong>
              </div>
            )}
            <table className="table table-bordered">
              <thead>
                <th width = "4%"></th>
                <th width="17%">
                  Резервивано от
                  <th>
                    <p></p>
                  </th>
                </th>
                <th width="15%">
                  Време на резервацията
                  <th>
                    <p></p>
                  </th>
                </th>
                <th width="15%">
                  Време на създаване
                  <th>
                    <p></p>
                  </th>
                </th>
               <th width="18%">Притежател
                   <th> <p></p>
                  </th></th>

                <th width="14%">
                  Паркомясто
                  <th width="7%">етаж</th>
                  <th width="7%">номер </th>
                </th>
                <th width="15%">
                  Свободно от
                  <th>
                    <p></p>
                  </th>
                </th>
                <th width="15%">
                  Свободно до
                  <th>
                    <p></p>
                  </th>
                </th>
                <th width = "7%"></th>

              </thead>
              <tbody>
                {this.state.reservations.map((reservation) => (
                  <tr key={reservation.uid}>
                    <td>
                      <img src={ReservationIcon}></img>
                    </td>
                    <td data-label="Резервирано от">
                        <img src={UserIcon}></img>
                        <p />
                        {reservation.reservedBy}
                      </td>
                    <td data-label="Време на резервиране">
                      <img src={ReservationTimeIcon}></img>
                      <p></p>
                      {new Intl.DateTimeFormat("en-GB", {
                        month: "long",
                        day: "2-digit",
                        year: "numeric",
                        hour: "2-digit",
                        minute: "2-digit",
                      }).format(new Date(reservation.reservationTime))}
                    </td>
                    <td data-label="Време на създаване">
                      <img src={CreatedAtIcon}></img>
                      <p />
                      {new Intl.DateTimeFormat("en-GB", {
                        month: "long",
                        day: "2-digit",
                        year: "numeric",
                        hour: "2-digit",
                        minute: "2-digit",
                      }).format(new Date(reservation.creationTime))}
                    </td>

                       <td data-label="Притежател">
                        <img src={UserIcon}></img>
                        <p />
                        {reservation.firstName} {reservation.lastName}
                        <p />
                        {reservation.email}
                      </td>
                    <td data-label="Паркомясто">
                      
                      <td data-label="етаж">
                        <img src={ParkingFloorIcon}></img>
                        {reservation.floor}
                      </td>
                      <td data-label="номер">
                        <img src={ParkingSlotIcon}></img>
                        {reservation.parkingSlot}
                      </td>
                    </td>
                    <td data-label="Свободно от">
                      <img src={FreeFromIcon}></img>
                      <p></p>
                      {new Intl.DateTimeFormat("en-GB", {
                        month: "long",
                        day: "2-digit",
                        year: "numeric",
                        hour: "2-digit",
                        minute: "2-digit",
                      }).format(new Date(reservation.freeFrom))}
                    </td>
                    <td data-label="Свободно до">
                      <img src={FreeToIcon}></img>
                      <p />
                      {new Intl.DateTimeFormat("en-GB", {
                        month: "long",
                        day: "2-digit",
                        year: "numeric",
                        hour: "2-digit",
                        minute: "2-digit",
                      }).format(new Date(reservation.freeTo))}
                    </td>
                    <td>
                      <IconButton>
                        <DeleteIcon
                          onClick={() =>
                            this.removeUserReservation(reservation.uid)
                          }
                        ></DeleteIcon>
                      </IconButton>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  }
}

export default Reservations;
