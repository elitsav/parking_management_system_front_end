import axios from "axios";

class UserService {

  getAllActiveUsers() {
    return axios.get("http://localhost:8090/user/find/all/active");
  }

getAllDeactivatedUsers() {
    return axios.get("http://localhost:8090/user/find/all/deactivated");
  }

  activateUser(userUid) {
    return axios.get("http://localhost:8090/user/activate/?userUid="+
        `${userUid}`);
  }
   deactivateUser(userUid) {
     return axios.delete("http://localhost:8090/user/deactivate/?userUid="+
        `${userUid}`);
  }
   getUserProfile(userUid) {
     return axios.get("http://localhost:8090/user/profile/"+
        `${userUid}`);
  }

}

export default new UserService();
