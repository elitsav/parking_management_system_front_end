import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/styles';
import axios from 'axios';
import PropTypes from 'prop-types';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const styles = theme => ({
  paper: {
    marginTop:80,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%',
  },
  submit: {
    spacing: [300, 0, 200],
  },
});

class ResendRegistrationToken extends React.Component {
    
  componentDidMount() {
     axios.get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
            const persons = res.data;
            this.setState({ persons });
       })
      }  
      
      constructor() {
        super();
        this.state = {
          successMsg: "",
          errorMsg: "",
          userEmail: "",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
      }
      handleChange = event=> {
        this.setState({[event.target.name]:event.target.value
      });

      }
    
      handleFormSubmit = event => {
        event.preventDefault();
    
        const userEmail = this.state.userEmail;
    
     axios.get( "http://localhost:8090/user/resendRegistrationToken/?userEmail=" +
        `${userEmail}`).then(res => {
              this.props.history.push("/");

      console.log(res);
      console.log(res.data)
    }).catch(error => {
      this.setState({
          errorMsg : error.response.data.message
      })
  });
      }
     
render(){
 const {classes} = this.props;
  return (
    <>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
       <Avatar className={classes.avatar}>
        </Avatar>
        <Typography component="h1" variant="h5">
          <mark>Resend registration token</mark>
        </Typography>
        <form className={classes.form} onSubmit={this.handleFormSubmit}>
        <p /><p><mark><center>If you did not receive  <i><b>verification code</b></i> to confirm your registration. 
        Please enter your email and we will send you again! </center></mark></p>
          <TextField onChange ={this.handleChange}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="userEmail"
            label="Email Address"
            name="userEmail"
            autoComplete="email"
            autoFocus
          />

          <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Request a new code
            </Button>
               {this.state.errorMsg && 
                    <div className="eli">
                      <i class="fa fa-exclamation-circle"></i>
                        <strong>{this.state.errorMsg}</strong>
                    </div>}
                    
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
    </>
  );
}
}
ResendRegistrationToken.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(ResendRegistrationToken);
