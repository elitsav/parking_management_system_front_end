// @flow

import React from "react";

import {
  Container,
  Grid,
  Card,
  Button,
  Form,
  Avatar,
  Profile,
  List,
  Media,
  Text,
  Comment,
} from "tabler-react";
import Layout from "../Layout";
import Homecss from "./Home.css";
import ProfilePicture from "../../images/profile_edit.png";
import Header from "../Header/HeaderComponent.js";
import AuthenticationService from "../../_services/AuthenticationService";
import axios from "axios";

class Home extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      successMsg: "",
      errorMsg: "",
      isLoading: false,
      email: "",
      firstName: "",
      lastName: "",
      phoneNumber: "",
      parkingSlot: "",
      floor: "",
    };

    this.getUserDetails = this.getUserDetails.bind(this);
    this.updateFirstName = this.updateFirstName.bind(this);
    this.handleFormSubmit=this.handleFormSubmit.bind(this);
    this.updateLastName=this.updateLastName.bind(this);
    this.updatePhoneNumber=this.updatePhoneNumber.bind(this);
  }

  getUserDetails() {
    this.setState({
      isLoading: true,
    });

    axios.get("http://localhost:8090/user/details").then((response) => {
      this.setState({
        isLoading: false,
        email: response.data.email,
        firstName: response.data.firstName,
        lastName: response.data.lastName,
        phoneNumber: response.data.phoneNumber,
        floor: response.data.floor,
        parkingSlot: response.data.parkingSlot,
      });
    });
  }
  updateFirstName(event) {
    this.setState({
      firstName: event.target.value,
    });
  }
   updateLastName(event) {
    this.setState({
      lastName: event.target.value,
    });
  }
 updatePhoneNumber(event) {
    this.setState({
      phoneNumber: event.target.value,
    });
  }
  handleFormSubmit = (event) => {
    event.preventDefault();

    const endpoint = "http://localhost:8090/user/profile/edit";

    const firstName = this.state.firstName;
    const lastName = this.state.lastName;
    const phoneNumber = this.state.phoneNumber;

    const user_object = {
      firstName: firstName,
      lastName: lastName,
      phoneNumber: phoneNumber,
    };
    axios.post(endpoint, user_object).then((res) => {
      this.setState({
          isLoading: false,
          successMsg: "Профилът е успешно редактиран!",
        });
      console.log(res);
      console.log(res.data);
    }).catch(() => {
        this.setState({
          errorMsg: `Възникна грешка! Моля, опитайте по-късно !`,
        });
      });;
  };
  componentDidMount() {
    AuthenticationService.setupAxiosInterceptors();
    this.getUserDetails();
  }

  render() {
    return (
      <>
      <Header></Header>
        <Layout></Layout>
        <div className="profile">
          <Container>
            <Grid.Col lg={10}>
              <div className="profile_form">
                <Form className="card" onSubmit={this.handleFormSubmit}>
                  <Card.Body>
                  {this.state.successMsg !== '' && 
                    <div className="alert alert-success alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.successMsg}</strong>
                    </div>}
                    {this.state.errorMsg !== '' && 
                    <div className="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.errorMsg}</strong>
                    </div>} 
                    <h3>Редактиране на профила</h3>
                    <center>
                      <img src={ProfilePicture}></img>
                    </center>
                    <Grid.Row>
                      <Grid.Col md={6}>
                        <Form.Group>
                          <Form.Label>Име:</Form.Label>
                          <Form.Input
                            type="text"
                            placeholder={this.state.firstName}
                            onChange={this.updateFirstName}
                          />
                        </Form.Group>
                      </Grid.Col>
                      <Grid.Col sm={6}>
                        <Form.Group>
                          <Form.Label>Фамилия:</Form.Label>
                          <Form.Input
                            type="text"
                            placeholder={this.state.lastName}
                            onChange={this.updateLastName}
                          />
                        </Form.Group>
                      </Grid.Col>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Col sm={8}>
                        <Form.Group>
                          <Form.Label>Email адрес:</Form.Label>
                          <Form.Input
                            type="email"
                            disabled
                            value={this.state.email}
                          />
                        </Form.Group>
                      </Grid.Col>
                    </Grid.Row>
                    <Grid.Row>
                      <Grid.Col sm={6} md={6}>
                        <Form.Group>
                          <Form.Label>Телефонен номер:</Form.Label>
                          <Form.Input
                            type="text"
                            placeholder={this.state.phoneNumber}
                            onChange={this.updatePhoneNumber}
                          />
                        </Form.Group>
                      </Grid.Col>
                    </Grid.Row>
                    <Form className="card" margin="300px">
                      <Grid.Row>
                        <Grid.Col sm={3}>
                          <label text-align="center">Паркомясто:</label>
                        </Grid.Col>
                      </Grid.Row>
                      <Grid.Row>
                        <Grid.Col sm={3}>
                          <Form.Group>
                            <Form.Label>Етаж:</Form.Label>
                            <Form.Input
                              type="text"
                              disabled
                              value={this.state.floor}
                            />
                          </Form.Group>
                        </Grid.Col>
                        <Grid.Col sm={3}>
                          <Form.Group>
                            <Form.Label>Номер:</Form.Label>
                            <Form.Input
                              type="text"
                              disabled
                              value={this.state.parkingSlot}
                            />
                          </Form.Group>
                        </Grid.Col>
                      </Grid.Row>
                    </Form>
                  </Card.Body>
                  <Card.Footer className="text-right">
                    <Button type="submit" color="primary">
                      Запази
                    </Button>
                  </Card.Footer>
                </Form>
              </div>
            </Grid.Col>
          </Container>
        </div>
      </>
    );
  }
}

export default Home;
