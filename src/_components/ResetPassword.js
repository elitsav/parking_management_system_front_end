import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/styles';
import axios from 'axios';
import PropTypes from 'prop-types';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const styles = theme => ({
  paper: {
    marginTop:80,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  form: {
    width: '100%'
  },
  submit: {
    spacing: [300, 0, 200],
  },
});

class ResetPassword extends React.Component {
    
  componentDidMount() {
     axios.get(`https://jsonplaceholder.typicode.com/users`)
      .then(res => {
            const persons = res.data;
            this.setState({ persons });
       })
      }  
      
      constructor() {
        super();
        this.state = {
          email: "",   
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
      }
      handleChange = event=> {
        this.setState({[event.target.name]:event.target.value})
      }
    
      handleFormSubmit = event => {
        event.preventDefault();
    
        const endpoint = "http://localhost:8090/user/resetPassword/";
    
        const email = this.state.email;

        axios.get(endpoint, {params: {email:email}}).then(res => {
          this.props.history.push("/user/password/change");
         console.log(res);
         console.log(res.data);
        });
      
      };
render(){
 const {classes} = this.props;
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
      
        <Typography component="h1" variant="h5">
          Забравена парола
        </Typography>
        <form className={classes.form} onSubmit={this.handleFormSubmit}>
        <p>Забрави ли сте паролата си? Моля въведете email адреса си. 
        Ще получите имейл за създаване на нова парола.</p>
          <TextField onChange ={this.handleChange}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="email"
            label="Email адрес"
            name="email"
            autoComplete="email"
            autoFocus
          />
          <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Ресетване на паролата            </Button>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}
}
ResetPassword.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(ResetPassword);
