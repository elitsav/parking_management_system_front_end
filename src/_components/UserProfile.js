
import React, { Component } from 'react';

import {
  Container,
  Grid,
  Card,
  Button,
  Form,
  Avatar,
  Profile,
  List,
  Media,
  Text,
  Comment,
} from "tabler-react";
import Layout from './Layout'
import Homecss from './EditProfile/Home.css'
import ProfilePicture from '../images/profile_picture.png'
import AuthenticationService from '../_services/AuthenticationService'
import axios from 'axios';
import Header from './Header/HeaderComponent'
import './MyProfile/MyProfile.css'
import UserService from '../_services/UserService'
import { Link } from 'react-router-dom';


class UserProfile extends React.Component{
      constructor(props) {
        super(props)

        this.state = {
            successMsg : '',
            errorMsg : '',
            isLoading : false,
            user :''
        }
        
      //  this.getUserProfile = this.getUserProfile.bind(this)
    }

    // getUserProfile(userUid) {
    //     // this.setState({
    //     //     isLoading : true
    //     // });

    //     UserService.getUserProfile(userUid)
    //     .then(res => {
    //       this.props.history.push("/");
    //       console.log(res);
    //       console.log(res.data);
    //     });
    // }

  componentDidMount() {
  const { match: { params } } = this.props;

  axios.get(`http://localhost:8090/user/profile/${params.userUid}`)
    .then(({ data: user }) => {
      console.log('user', user);

      this.setState({ user });
    });
}

  render() {  
  return (
    <>
    <Header></Header>
    <Layout></Layout>
      <div className="profile">
        <Container>
            <Grid.Col  lg={10}>
            <div className = "profile_form">
            <div></div>
              <Form >
                <Card.Body className="card">
                  <h3>Профилът на {this.state.user.firstName + ' ' +this.state.user.lastName}</h3>
                  <center><img src = {ProfilePicture}></img></center>
                  <Grid.Row>
                    <Grid.Col md={6}>
                      <Form.Group>
                        <Form.Label>First name:</Form.Label>
                        <Form.Input
                          type="text"
                          value ={this.state.user.firstName}
                          disabled
                        />
                      </Form.Group>
                    </Grid.Col>
                    <Grid.Col sm={6}>
                      <Form.Group>
                        <Form.Label>Last Name:</Form.Label>
                        <Form.Input
                          type="text"
                          value ={this.state.user.lastName}
                          disabled
                        />
                      </Form.Group>
                    </Grid.Col>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Col sm={8}>
                      <Form.Group>
                        <Form.Label>Email:</Form.Label>
                        <Form.Input 
                        type="email" 
                        value ={this.state.user.email}
                        disabled
                        />
                      </Form.Group>
                    </Grid.Col>
               </Grid.Row>
               <Grid.Row>    
                    <Grid.Col sm={6} md={6}>
                      <Form.Group>
                        <Form.Label>Phone number:</Form.Label>
                        <Form.Input
                          type="text"
                          disabled
                          value ={this.state.user.phoneNumber}
                        />
                      </Form.Group>
                    </Grid.Col>
                </Grid.Row> 
                 <Form className="card">
                 <Grid.Row> 
                 <Grid.Col sm={3} >
                   <Form.Group>
                       <Form.Label text-align="center">Parking Slot:</Form.Label>
                    </Form.Group>
                 </Grid.Col>
                  </Grid.Row> 
                <Grid.Row>                  
                    <Grid.Col sm={3} >
                      <Form.Group>
                        <Form.Label>Floor:</Form.Label>
                        <Form.Input
                          disabled
                          value ={this.state.user.floor}
                        />
                      </Form.Group>
                    </Grid.Col>
                    <Grid.Col sm={3} >
                      <Form.Group>
                        <Form.Label>Number:</Form.Label>
                        <Form.Input
                          disabled
                          value ={this.state.user.parkingSlot}
                        />
                      </Form.Group>
                    </Grid.Col>
                </Grid.Row>
                </Form>    
                </Card.Body>
                <Card.Footer className="text-right">
                </Card.Footer>
              </Form>
              </div>
            </Grid.Col>
        </Container>
      </div>
      </>
  );
}
}

export default UserProfile;