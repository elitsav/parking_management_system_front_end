import React, {Component} from 'react'
import {Link} from 'react-router-dom'
import {withRouter} from 'react-router'
import {
 Button,
 Dropdown,
 AccountDropdown
} from "tabler-react";
import AuthenticationService from '../../_services/AuthenticationService';

import './HeaderComponent.css'

class Header extends Component {
    
    render() {

        const isUserLoggedIn = AuthenticationService.isUserLoggedIn();
        const isAdmin = AuthenticationService.isAdmin();

        return(
            <>
            <header className = "header">
                <nav className="navbar navbar-expand-md navbar-default">

          
                <ul className="navbar-nav navbar-collapse justify-content-end">
                    
                    {isAdmin && <Button.Dropdown value="Админ-Меню" aria-hidden="true">
                     <Dropdown.Item  href="/user/deactivate">Активни потребители</Dropdown.Item>
                     <Dropdown.Item href="/user/activate">Деактивирани потребители</Dropdown.Item>
                     <Dropdown.ItemDivider />
                     <Dropdown.Item href="/parkingSlots">Паркоместа & Притежатели</Dropdown.Item>
                     <Dropdown.Item href="/create">Добави паркомясто</Dropdown.Item>
                     <Dropdown.ItemDivider />
                     <Dropdown.Item href="/statistics">Статистики</Dropdown.Item>
                     </Button.Dropdown>
                     }           
                    </ul>
                     <ul className="navbar-nav navbar-collapse justify-content-end">
                        <center> Добре дошли в Parking Management System !</center>
                    </ul>
                    <ul className="navbar-nav navbar-collapse justify-content-end">
                        {isUserLoggedIn && <i className="fa fa-sign-out" aria-hidden="true"></i>}
                        {isUserLoggedIn && <li><Link className="nav-link" to="/logout" 
                            onClick={AuthenticationService.successfulLogout}>Изход</Link></li>}
                        <i className="fa fa-sign-out" aria-hidden="true"></i>
                    </ul>
                </nav>
            </header>
            </>
        )
    }
}

export default withRouter(Header)