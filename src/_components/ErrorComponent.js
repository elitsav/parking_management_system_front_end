import React, {Component} from 'react'

import ErrorPage from '../images/error_page.jpg'

class ErrorComponent extends Component {

    render() {
        return (
            <div className="errorPage">
                <center><img src={ErrorPage} alt="error"></img></center>
            </div>
        )
    }
}

export default ErrorComponent