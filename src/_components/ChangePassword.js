import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { withStyles } from '@material-ui/styles';
import axios from 'axios';
import PropTypes from 'prop-types';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const styles = theme => ({
  paper: {
    //marginTop: theme.spacing(8),
    marginTop:80,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  // avatar: {
  //   // margin: theme.spacing(1),
  //   spacing:100,
  //   backgroundColor: '#f48fb1'
  // },
  form: {
    width: '100%', // Fix IE 11 issue.
    // marginTop: theme.spacing(1),
    // marginTop:100,

  },
  submit: {
    spacing: [300, 0, 200],
  },
});

class ChangePassword extends React.Component {
      
      constructor() {
        super();
        this.state = {
          token: "",   
          newPassword:"",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleFormSubmit = this.handleFormSubmit.bind(this);
      }
      handleChange = event=> {
        this.setState({ [event.target.name]:event.target.value,
                        [event.target.name]:event.target.value,
                        [event.target.name]:event.target.value})
      }
    
      handleFormSubmit = event => {
        event.preventDefault();
    
        const endpoint = "http://localhost:8090/user/changePassword";

        const token = this.state.token;
        const newPassword = this.state.newPassword;

    const user_object = {
        token: token,
        newPassword: newPassword

    };
        axios.post(endpoint, user_object).then(res => {
            // localStorage.setItem("authorization", res.data.token);
            // return this.handleDashboard();
            this.props.history.push("/");
            console.log(res);
            console.log(res.data);
          });
      
      };
render(){
 const {classes} = this.props;
  return (
    <>
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          </Avatar>
        <Typography component="h1" variant="h5">
           Смяна на парола
        </Typography>
        <form className={classes.form} onSubmit={this.handleFormSubmit}>
        <p>Използвайте тоукъна, който е изпратен до вашия email адрес и сменете вашата парола.</p>
          <TextField onChange ={this.handleChange}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="token"
            label="Ресет тоукън"
            name="token"
            autoComplete="token"
            autoFocus
          />
          <TextField onChange ={this.handleChange}
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="newPassword"
            label="Нова парола"
            name="newPassword"
            autoComplete="newPassword"
            type="password"
            autoFocus
          />
          <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Смяна на парола
            </Button>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
    </>
  );
}
}
ChangePassword.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(ChangePassword);
