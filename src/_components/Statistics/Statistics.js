
// @flow

import React from "react";

import {
  Container,
  Grid,
  Card,
  Button,
  Form,
  Avatar,
  Profile,
  List,
  Media,
  StatsCard,
  Text,
  Page,
  Comment,
} from "tabler-react";
import Layout from "../Layout";
import ProfilePicture from "../../images/profile_edit.png";
import Header from "../Header/HeaderComponent.js";
import AuthenticationService from "../../_services/AuthenticationService";
import axios from "axios";
import ReservationService from "../../_services/ReservationService.js";
import './Statistics.css'

class Statistics extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      successMsg: "",
      errorMsg: "",
      isLoading: false,
      statistics :[],
    };
       this.getStatistics=this.getStatistics.bind(this);
  }
    getStatistics() {
    AuthenticationService.setupAxiosInterceptors();
    // this.setState({
    //     isLoading : true
    // });

    ReservationService.getStatistics().then((response) => {
      this.setState({
        isLoading: false,
        statistics: response.data,
      });
    });
  }
  componentDidMount(){
      this.getStatistics();
  }

  render() {
    return (
      <>
      
      <Header></Header>
        <Layout></Layout>
  <Page.Content className = "title">
        <Grid.Row className= "cards" cards={true}>
          <Grid.Col width={6} sm={4} lg={3}>
            <StatsCard layout={1}  movement={"+"}  total={this.state.statistics.allUsers} label="Потребители" ></StatsCard>
          </Grid.Col>
          <Grid.Col width={6} sm={4} lg={3}>
            <StatsCard layout={1} movement={"+"} total={this.state.statistics.allAnnouncements} label="Свободни паркоместа" />
          </Grid.Col>
          <Grid.Col width={6} sm={4} lg={3}>
            <StatsCard
              layout={1}
              movement={3}
              total={this.state.statistics.allReservations}
              label="Резервации"
            />
          </Grid.Col>
        </Grid.Row >
                  </Page.Content>

      </>
    );
  }
}

export default Statistics;
