import axios from "axios";

class AnnouncementService {


  getAllAnnouncements() {
    return axios.get("http://localhost:8090/announcement/find/all");
  }

  getAllAnnouncementsCreatedByUser() {
    return axios.get("http://localhost:8090/user/announcements");
  }

  reserveAnnouncement(announcementUid) {
    return axios.get(
      "http://localhost:8090/parkingSlot/reserve/?announcementUid=" +
        `${announcementUid}`
    );
  }
  removeAnnouncement(announcementUid) {
    return axios.delete(
      "http://localhost:8090/announcement/remove/?announcementUid=" +
        `${announcementUid}`
    );
  }
}

export default new AnnouncementService();
