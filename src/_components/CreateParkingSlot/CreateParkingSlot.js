import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import moment from 'moment';
import axios from 'axios';
import  Datetime from 'react-datetime';
import Header from "../Header/HeaderComponent.js";
import 'react-datepicker/dist/react-datepicker.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import AuthenticationService from '../../_services/AuthenticationService'
import { DateTimePicker, KeyboardDateTimePicker  } from '@material-ui/pickers'
import { MuiPickersUtilsProvider, InlineDatePicker } from '@material-ui/pickers';
import DateFnsUtils from "@date-io/date-fns";
import Icon from "@material-ui/core/Icon";
import Layout from '../Layout'
import QuestionMark from '../../images/question_mark.jpg'
import ParkingIcon from '../../images/parking-car.png'
import { Formik, Form, Field } from "formik";
import TextField from '@material-ui/core/TextField';
import './CreateParkingSlot.css'


class CreateParkingSlot extends Component {

  constructor (props) {
    super(props)
    this.state = {
      floor: '',
      number:'',
      successMsg : '',
      errorMsg : '',
    };
    this.onhandleSubmit = this.onhandleSubmit.bind(this);
    this.handleChange=this.handleChange.bind(this);
  }
   handleChange = event=> {
    this.setState({[event.target.name]:event.target.value, [event.target.name]:event.target.value})
  }

  onhandleSubmit(values) {
    let floor = this.state.floor;
    let number = this.state.number;
    const dateObj = {
      floor: floor,
      number:number
    }
     AuthenticationService.setupAxiosInterceptors()
    axios.post('http://localhost:8090/parkingSlot/add', dateObj)
        // .then(res => console.log(res.data));

        .then(res => {
            this.setState({
                isLoading : false,
                successMsg : "Паркомястото е успешно добавено!"
            })            
        })
        .catch(() => {
            this.setState({
                errorMsg : `Възникна грешка! Моля, опитайте по-късно  !`
            })
        })
  }


  render() {
  
    
        let { floor, number } = this.state

        return (
          <>
          <Header></Header>
          <Layout></Layout>
            <div className="forgotPassword">
                <Formik 
                    initialValues={{floor,number}}
                    onSubmit={this.onSendResetPasswordEmailClick}
                    validate={this.validate}
                    validateOnChange={true}
                    enableReinitialize={true}
                    > 
                    {
                        ({ errors, touched }) => (
                            <Form>
                                <h2><b><mark>Добави паркомясто:</mark></b></h2>
                                {this.state.successMsg !== '' && 
                                <div className="alert alert-success alert-dismissible" role="alert">
                                    <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>{this.state.successMsg}</strong>
                                </div>}
                                {this.state.errorMsg !== '' && 
                                <div className="alert alert-danger alert-dismissible" role="alert">
                                    <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <strong>{this.state.errorMsg}</strong>
                                </div>}  
                                <Field as={TextField} className="form-control" placeholder="Етаж" type="text" name="floor" 
                                    autoComplete="off" id="outlined-textarea" variant="outlined" label="Етаж" onChange ={this.handleChange}></Field>
                                 {touched.floor && errors.floor && <div className="errorField">{errors.floor}</div>}
                                     <Field as={TextField} className="form-control" placeholder="Номер" type="text" name="number" 
                                    autoComplete="off" id="outlined-textarea" variant="outlined" label="Номер"onChange ={this.handleChange}></Field>
                                {touched.number && errors.number && <div className="errorField">{errors.number}</div>}
                                <button type="submit" className="sendResetPasswordEmailBtn" onClick = {this.onhandleSubmit}>
                                    <span>Добави</span></button>
                            </Form>
                            
                            )
                        }
                </Formik>
            </div>
            </>
        )
  }
}

export default CreateParkingSlot;