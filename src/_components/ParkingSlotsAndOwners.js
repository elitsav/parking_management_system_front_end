import React, { Component } from "react";

import { IconButton } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import EmailIcon from "@material-ui/icons/Email";
import ReservationService from "../_services/ReservationService.js";
import ParkingSlotService from "../_services/ParkingSlotService.js";
import AuthenticationService from "../_services/AuthenticationService.js";
import Layout from "./Layout";
import LoadingIndicator from './LoadingIndicator'
import ReservationImage from "../images/reservations.jpg";
import CreatedAtIcon from "../images/calendar.png";
import FreeFromIcon from "../images/free_from.png";
import FreeToIcon from "../images/free_to.png";
import ParkingSlotIcon from "../images/parking_slot.png";
import ParkingFloorIcon from "../images/parking_floor.png";
import ReservationIcon from "../images/reservation_icon.png";
import ReservationTimeIcon from "../images/reservation_time.png";
import UserIcon from "../images/user_icon.png";
import Header from "./Header/HeaderComponent.js";
import axios from 'axios';
import "./MyReservations/MyReservations.css";
import { Link } from 'react-router-dom';


class ParkingSlotsAndOwners extends Component {
  constructor(props) {
    super(props);

    this.state = {
      successMsg: "",
      errorMsg: "",
      isLoading: false,
      parkingSlots: []
    };

    this.getAllParkingSlotsAndTheirUsers = this.getAllParkingSlotsAndTheirUsers.bind(
      this
    );
    // this.removeUserReservation=this.removeUserReservation.bind(this)
  }

  componentDidMount() {
    this.getAllParkingSlotsAndTheirUsers();
  }

  getAllParkingSlotsAndTheirUsers() {
    AuthenticationService.setupAxiosInterceptors();
    this.setState({
        isLoading : true
    });

    axios.get("http://localhost:8090/parkingSlots").then((response) => {
      this.setState({
        isLoading: false,
        parkingSlots: response.data,
      });
    });
  }

  removeParkingSlot(parkingSlotUid) {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });
  
        ParkingSlotService.removeParkingSlot(parkingSlotUid)
        .then(response => {
            this.setState({
                isLoading : false,
                successMsg : "Паркоместото е успешно премахнато!"
            })
            this.getAllParkingSlotsAndTheirUsers()
            
        })
        .catch(() => {
            this.setState({
                errorMsg : `Възникна грешка! Моля опитайте по-късно ! !`
            })
        })
    }

  render() {
    if(this.state.isLoading) {
        return <LoadingIndicator/>
    }
    return (
      <>
        <Header></Header>
        <Layout />
        <div className="reservations">
          <div className="bs-example">
           <center> <h1>ПАРКОМЕСТА & ПРИТЕЖАТЕЛИ </h1></center>

            {this.state.successMsg !== "" && (
              <div
                className="alert alert-success alert-dismissible"
                role="alert"
              >
                <button
                  type="button"
                  className="close"
                  data-dismiss="alert"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong>{this.state.successMsg}</strong>
              </div>
            )}
            {this.state.errorMsg !== "" && (
              <div
                className="alert alert-danger alert-dismissible"
                role="alert"
              >
                <button
                  type="button"
                  className="close"
                  data-dismiss="alert"
                  aria-label="Close"
                >
                  <span aria-hidden="true">&times;</span>
                </button>
                <strong>{this.state.errorMsg}</strong>
              </div>
            )}
            <table className="table table-bordered">
              <thead>
                <th width="15%">
                  Етаж                
                </th>
                <th width="15%">
                 Номер               
                </th>
               <th width="8%">Статус
                   </th>     
                <th width="15%">
                  Притежател
                </th>
                <th width ="5%"></th>
              </thead>
              <tbody>
              {
                 this.state.parkingSlots.map(
                 parkingSlot =>                  
                  <tr key={parkingSlot.uid}>
                    <td data-label="Етаж">
                        <img src={ParkingFloorIcon}></img>
                            <mark>{parkingSlot.floor}</mark>
                      </td>
                      <td data-label="Номер">
                        <img src={ParkingSlotIcon}></img>
                        <mark>{parkingSlot.number}</mark>
                      </td>
                      <td data-label="Статус">
                      <b> <i><mark> {parkingSlot.status}</mark></i></b>
                      </td>
                       <td data-label="Притежател">
                        <img src={UserIcon}></img>
                        <p />
                        
                        <Link to={`/user/details/${parkingSlot.userUid}`}>{parkingSlot.email} </Link>
                      </td>
                    
                    
                    <td>
                      <IconButton>
                        <DeleteIcon
                          onClick={() =>
                            this.removeParkingSlot(parkingSlot.uid)
                          }
                        ></DeleteIcon>
                      </IconButton>
                    </td>
                  </tr>
                )}
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  }
}

export default ParkingSlotsAndOwners;
