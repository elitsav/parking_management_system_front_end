
import React, { Component } from 'react';

import {
  Container,
  Grid,
  Card,
  Button,
  Form,
  Avatar,
  Profile,
  List,
  Media,
  Text,
  Comment,
} from "tabler-react";
import Layout from '../Layout'
import Homecss from '../EditProfile/Home.css'
import ProfilePicture from '../../images/profile_picture.png'
import AuthenticationService from '../../_services/AuthenticationService'
import axios from 'axios';
import Header from '../Header/HeaderComponent'
import './MyProfile.css'

class MyProfile extends React.Component{
      constructor(props) {
        super(props)

        this.state = {
            successMsg : '',
            errorMsg : '',
            isLoading : false,
            user :''
        }
        
       this.getUserDetails = this.getUserDetails.bind(this)
    }

    getUserDetails() {
        // this.setState({
        //     isLoading : true
        // });

        (axios.get("http://localhost:8090/user/details"))
        .then(response => {
            this.setState({
                isLoading : false,
                user : response.data
            })
        })
    }

  componentDidMount() {
    AuthenticationService.setupAxiosInterceptors()
    this.getUserDetails()
  }  

  render() {  
  return (
    <>
    <Header></Header>
    <Layout></Layout>
      <div className="profile">
        <Container>
            <Grid.Col  lg={10}>
            <div className = "profile_form">
            <div></div>
              <Form >
                <Card.Body className="card">
                  <h3>МОЯТ ПРОФИЛ</h3>
                  <center><img src = {ProfilePicture}></img></center>
                  <Grid.Row>
                    <Grid.Col md={6}>
                      <Form.Group>
                        <Form.Label>Име:</Form.Label>
                        <Form.Input
                          type="text"
                          value ={this.state.user.firstName}
                          disabled
                        />
                      </Form.Group>
                    </Grid.Col>
                    <Grid.Col sm={6}>
                      <Form.Group>
                        <Form.Label>Фамилия:</Form.Label>
                        <Form.Input
                          type="text"
                          value ={this.state.user.lastName}
                          disabled
                        />
                      </Form.Group>
                    </Grid.Col>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Col sm={8}>
                      <Form.Group>
                        <Form.Label>Email адрес:</Form.Label>
                        <Form.Input 
                        type="email" 
                        value ={this.state.user.email}
                        disabled
                        />
                      </Form.Group>
                    </Grid.Col>
               </Grid.Row>
               <Grid.Row>    
                    <Grid.Col sm={6} md={6}>
                      <Form.Group>
                        <Form.Label>Телефонен номер:</Form.Label>
                        <Form.Input
                          type="text"
                          disabled
                          value ={this.state.user.phoneNumber}
                        />
                      </Form.Group>
                    </Grid.Col>
                </Grid.Row> 
                 <Form className="card">
                 <Grid.Row> 
                 <Grid.Col sm={5} >
                   <Form.Group>
                       <Form.Label text-align="center">Паркомясто:</Form.Label>
                    </Form.Group>
                 </Grid.Col>
                  </Grid.Row> 
                <Grid.Row>                  
                    <Grid.Col sm={3} >
                      <Form.Group>
                        <Form.Label>Етаж:</Form.Label>
                        <Form.Input
                          disabled
                          value ={this.state.user.floor}
                        />
                      </Form.Group>
                    </Grid.Col>
                    <Grid.Col sm={3} >
                      <Form.Group>
                        <Form.Label>Номер:</Form.Label>
                        <Form.Input
                          disabled
                          value ={this.state.user.parkingSlot}
                        />
                      </Form.Group>
                    </Grid.Col>
                </Grid.Row>
                </Form>    
                </Card.Body>
                <Card.Footer className="text-right">
                </Card.Footer>
              </Form>
              </div>
            </Grid.Col>
        </Container>
      </div>
      </>
  );
}
}

export default MyProfile;