import React from 'react';
import { useIntl } from 'react-intl';
import { Link } from 'react-router-dom';
import UserIcon from '../images/user_profile.png'
import 'react-pro-sidebar/dist/css/styles.css';

import {
  ProSidebar,
  Menu,
  MenuItem,
  SubMenu,
  SidebarHeader,
  SidebarFooter,
  SidebarContent,
} from 'react-pro-sidebar';
import { FaTachometerAlt, FaGem, FaList, FaGithub, FaRegLaughWink, FaHeart } from 'react-icons/fa';
import sidebarBg from '../assets/bg1.jpg';

const Aside = ({ image, collapsed, rtl, toggled, handleToggleSidebar }) => {
  const intl = useIntl();
  return (

    <ProSidebar
     image={image ? sidebarBg : false}
      rtl={rtl}
      collapsed={collapsed}
      toggled={toggled}
      breakPoint="md"
      onToggle={handleToggleSidebar}
      
    >
    <SidebarHeader>
        <div >
          <h6 style={{
            textTransform: 'uppercase',
            fontWeight: 'bold',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
            textAlign: 'center'
          }}><p /><em >Parking Management   <p style={{textAlign: 'center'}}>System</p></em></ h6>
        </div>
      </SidebarHeader>
      <SidebarHeader>
        <div
          style={{
            padding: '24px',
            fontWeight: 'bold',
            fontSize: 14,
            letterSpacing: '1px',
            overflow: 'hidden',
            textOverflow: 'ellipsis',
            whiteSpace: 'nowrap',
          }} >
           <img   src={UserIcon}></img>
           {localStorage.getItem("email")}
        </div>
      </SidebarHeader>

      <SidebarContent>
      
        <Menu iconShape="circle">
        <SubMenu
            suffix={<span className="badge yellow">3</span>}
            title={intl.formatMessage({ id: 'withSuffix' })}
            icon={<FaRegLaughWink />}
          >
            <MenuItem>За мен <Link to= "/user/profile"/> </MenuItem>
           <MenuItem>Редактиране на профила <Link to= "/user/profile/edit"/> </MenuItem>
            <MenuItem>Моите обяви <Link to= "/user/announcements"/></MenuItem>
            <MenuItem>Моите резервации <Link to= "/user/reservations"/></MenuItem>
            <MenuItem> Смяна на парола<Link to= "/user/password/change"/></MenuItem>
          </SubMenu>
          <MenuItem
            icon={<FaList />}
            suffix={<span className="badge red"></span>}
          >
            Свободни паркоместа
            <Link to= "/announcements"/>
          </MenuItem>
          <MenuItem icon={<FaGem />}> {intl.formatMessage({ id: 'components' })}  <Link to= "/announcement/create"/></MenuItem>
        </Menu>
        <Menu iconShape="circle">
          
          {/* <SubMenu
            prefix={<span className="badge gray">3</span>}
            title={intl.formatMessage({ id: 'withPrefix' })}
            icon={<FaHeart />}
          >
            <MenuItem>{intl.formatMessage({ id: 'submenu' })} 1</MenuItem>
            <MenuItem>{intl.formatMessage({ id: 'submenu' })} 2</MenuItem>
            <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3</MenuItem>
          </SubMenu>
          <SubMenu title={intl.formatMessage({ id: 'multiLevel' })} icon={<FaList />}>
            <MenuItem>{intl.formatMessage({ id: 'submenu' })} 1 </MenuItem>
            <MenuItem>{intl.formatMessage({ id: 'submenu' })} 2 </MenuItem>
            <SubMenu title={`${intl.formatMessage({ id: 'submenu' })} 3`}>
              <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.1 </MenuItem>
              <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.2 </MenuItem>
              <SubMenu title={`${intl.formatMessage({ id: 'submenu' })} 3.3`}>
                <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.3.1 </MenuItem>
                <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.3.2 </MenuItem>
                <MenuItem>{intl.formatMessage({ id: 'submenu' })} 3.3.3 </MenuItem>
              </SubMenu>
            </SubMenu> */}
          {/* </SubMenu> */}
        </Menu>
      </SidebarContent>

      <SidebarFooter style={{ textAlign: 'center' }}>
        {/* <div
          className="sidebar-btn-wrapper"
          style={{
            padding: '20px 24px',
          }}
        >
          <a
            href="https://github.com/azouaoui-med/react-pro-sidebar"
            target="_blank"
            className="sidebar-btn"
            rel="noopener noreferrer"
          >
            <FaGithub />
            <span> {intl.formatMessage({ id: 'viewSource' })}</span>
          </a>
        </div> */}
      </SidebarFooter>
    </ProSidebar>
  );
};

export default Aside;