import axios from "axios";

class ReservationService {

   getAllReservationsCreatedByUser() {
    return axios.get("http://localhost:8090/user/reservations");
  }

 getAllReservations() {
    return axios.get("http://localhost:8090/reservation/find/all");
  }

  removeUserReservation(reservationUid) {
    return axios.delete(
      "http://localhost:8090/user/reservation/remove/?reservationUid=" +
        `${reservationUid}`
    );
  }
  getStatistics
  getStatistics() {
    return axios.get("http://localhost:8090/statistics");
  }
}

export default new ReservationService();
