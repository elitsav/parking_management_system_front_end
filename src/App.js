import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import SignInSide from './_components/SignInSide';
import SignUp from './_components/SignUp';
import ResetPassword from './_components/ResetPassword' ;
import ChangePassword from './_components/ChangePassword' ;
import ConfirmRegistration from './_components/ConfirmRegistration' ;
import interceptors from "./interceptors";
import dashboard from "./_components/Dashboard";
import { BrowserRouter as Router, Route, Switch, BrowserRouter } from "react-router-dom";
import { history } from './history';
import { Alert } from './_components/Alert';
import Announcement from './_components/Announcement/Announcement'
import Home from './_components/EditProfile/Home'
import CreateAnnouncement from './_components/CreateAnnouncement/CreateAnnouncement'
import  axios from 'axios';
import HeaderComponent from './_components/Header/HeaderComponent'
import {IntlProvider} from 'react-intl';
import './styles/App.scss';
import Layout from './_components/Layout';
import messages from './messages';
import MyProfile from './_components/MyProfile/MyProfile'
import MyAnnouncements from './_components/MyAnnouncements'
import MyReservations from './_components/MyReservations/MyReservations'
import AuthenticationRoute from './_components/AuthenticationRoute.js'
import ErrorComponent from './_components/ErrorComponent'
import "tabler-react/dist/Tabler.css"
import User from './_components/User.js'
import DeactivatedUsers from './_components/DeactivatedUsers'
import Statistics from './_components/Statistics/Statistics'
import Reservations from './_components/Reservations'
import UserProfile from './_components/UserProfile'
import ParkingSlotsAndOwners from './_components/ParkingSlotsAndOwners'
import CreateParkingSlot from './_components/CreateParkingSlot/CreateParkingSlot'
import EditUserProfileForAdmin from './_components/EditProfile/EditUserProfileForAdmin'
import ResendRegistrationToken from './_components/ResendRegistrationToken'
import LogoutComponent from './_components/LogoutComponent'

function App() {

      const [locale, setLocale] = useState('en');


  return (

   
      <IntlProvider locale={locale} messages={messages[locale]}>

      <Router>
          {/* <HeaderComponent></HeaderComponent> */}
          <Switch>
          {/* <Alert /> */}
          <Route exact path="/" component={SignInSide} />
          <Route exact path="/register" component={SignUp} />
          <Route exact path="/resetPassword" component={ResetPassword} />
          <Route exact path="/user/password/change" component={ChangePassword} />
          <Route exact path="/user/registration/confirm" component={ConfirmRegistration} />
          <AuthenticationRoute exact path="/announcements" component={Announcement}/>
          <AuthenticationRoute exact path="/announcement/create" component={CreateAnnouncement}/>
          <AuthenticationRoute exact path="/navbar" component={Layout}/>
          <AuthenticationRoute exact path="/user/profile/edit" component={Home}/>
          <AuthenticationRoute exact path="/user/profile" component={MyProfile}/>
          <AuthenticationRoute exact path="/user/announcements" component={MyAnnouncements}/>
          <AuthenticationRoute exact path="/user/reservations" component={MyReservations}/>
          <AuthenticationRoute exact path="/user/deactivate" component={User}/>
          <AuthenticationRoute exact path="/user/activate" component={DeactivatedUsers}/>
          <AuthenticationRoute exact path="/statistics" component={Statistics}/>
          <AuthenticationRoute exact path="/reservations" component={Reservations}/>
          <AuthenticationRoute  path="/user/details/:userUid" component={UserProfile}></AuthenticationRoute>
          <AuthenticationRoute  path="/parkingSlots" component={ParkingSlotsAndOwners}></AuthenticationRoute>
          <AuthenticationRoute  path="/create" component={CreateParkingSlot}></AuthenticationRoute>
          <AuthenticationRoute  path="/admin/user/profile/edit/:userUid" component={EditUserProfileForAdmin}></AuthenticationRoute>
          <Route  path="/logout" component={LogoutComponent}></Route>
          <Route component={ErrorComponent}></Route>



     </Switch>
      </Router>

     </IntlProvider>

  );
}


export default App;
