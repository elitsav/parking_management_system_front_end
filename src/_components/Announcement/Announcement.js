import React, {Component} from 'react'

import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EmailIcon from '@material-ui/icons/Email';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import LocalParkingIcon from '@material-ui/icons/LocalParking';
import BusinessIcon from '@material-ui/icons/Business';
import HeaderComponent from '../Header/HeaderComponent.js';
import UserService from '../../_services/UserService.js'
import AuthenticationService from '../../_services/AuthenticationService.js'
import AnnouncementService from '../../_services/AnnouncementService.js'
import LoadingIndicator from '../LoadingIndicator'
import BookImage from '../../images/booking_btn.png'
import CreatedAtIcon from '../../images/calendar.png'
import FreeFromIcon from '../../images/free_from.png'
import FreeToIcon from '../../images/free_to.png'
import ParkingSlotIcon from '../../images/parking_slot.png'
import ParkingFloorIcon from '../../images/parking_floor.png'
import UserIcon from '../../images/user_icon.png'
import Layout from '../Layout';
import Header from '../Header/HeaderComponent.js'
import './Announcement.css'
import { Link } from 'react-router-dom';
import axios from "axios";


class Announcement extends Component {

    constructor(props) {
        super(props)

        this.state = {
            successMsg : '',
            errorMsg : '',
            isLoading : false,
            announcements : []
        }
        
        this.getAllAnnoucements = this.getAllAnnoucements.bind(this)
        this.reserveAnnouncement=this.reserveAnnouncement.bind(this)
        this.removeAnnouncement=this.removeAnnouncement.bind(this)
        this.getUserProfile=this.getUserProfile.bind(this)
    }

    componentDidMount() {
        this.getAllAnnoucements()
    }

    getAllAnnoucements() {
       AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });

        AnnouncementService.getAllAnnouncements()
        .then(response => {
            this.setState({
                isLoading : false,
                announcements : response.data
            })
        })
    }

   reserveAnnouncement(announcementUid) {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });
    
        AnnouncementService.reserveAnnouncement(announcementUid)
        .then(response => {
            this.setState({
                isLoading : false,
                successMsg : "Успешно запазихте паркомясто!"
            })
            this.getAllAnnoucements()
        })
        .catch(() => {
            this.setState({
                errorMsg : `Възникна грешка! Моля, опитайте по-късно !`
            })
        })
    }
     removeAnnouncement(announcementUid) {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });
    
        AnnouncementService.removeAnnouncement(announcementUid)
        .then(response => {
            this.setState({
                isLoading : false,
                successMsg : "Обявата е успешно премахната!"
            })
            this.getAllAnnoucements()
        })
        .catch(() => {
            this.setState({
                errorMsg : `Възникна грешка! Моля, опитайте по-късно !`
            })
        })
    }
     getUserProfile(userUid) {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });
    
        (axios.get("http://localhost:8090/user/profile/"+
        `${userUid}`))
        .then(res => {
          this.props.history.push("/");
          console.log(res);
          console.log(res.data);
        });
    }
    render() {
        if(this.state.isLoading) {
           return <LoadingIndicator/>
        }
        const isAdmin = AuthenticationService.isAdmin();
        return (
            <>
            <Header></Header>
            <Layout />
            <div className="announcements">
                <div className="bs-example">
                    {this.state.successMsg !== '' && 
                    <div className="alert alert-success alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.successMsg}</strong>
                    </div>}
                    {this.state.errorMsg !== '' && 
                    <div className="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.errorMsg}</strong>
                    </div>} 
                    <h1>Свободни паркоместа</h1>
                    <table className="table table-hover">
                        <thead>
                                <th color="green" width='20%'>Дата на създаване<th><p></p></th></th>
                                <th width='20%'>Притежател<th><p></p></th></th>
                                <th width= '14%'>Паркомясто
                                    <th  width='7%'>етаж</th>
                                    <th  width='7%'>номер </th></th>
                                <th  width='15%'>Свободно от<th><p></p></th></th>
                                <th  width='15%'>Свободно до<th><p></p></th></th>
                                <th  width='15%'></th>
                        </thead>
                        <tbody>
                        {
                            this.state.announcements.map(
                                announcement => 
                                    <tr key={announcement.uid}>
                                        <td data-label="Дата на създаване" >
                                        <img   src={CreatedAtIcon}></img>
                                        <p />
                                                {new Intl.DateTimeFormat('en-GB', { 
                                                month: 'long', 
                                                day: '2-digit',
                                                year: 'numeric', 
                                                 hour: '2-digit',
                                                minute: '2-digit'
                                            }).format(new Date(announcement.creationTime))}
                                        </td>
                                        <td data-label="Притежател">
                                            <Link to={`/user/details/${announcement.userUid}`}>
                                       <img   src={UserIcon}></img><p />
                                       {announcement.firstName}{' '}
                                       {announcement.lastName}<p />
                                        {announcement.email} </Link></td>
                                        <td data-label = "Паркомясто">
                                        <td data-label="етаж" >
                                        <img   src={ParkingFloorIcon}></img>
                                        {announcement.floor}</td>
                                        <td data-label="номер">
                                        <img   src={ParkingSlotIcon}></img>
                                        {announcement.parkingSlot}</td></td>
                                        <td data-label="Свободно от">
                                        <img   src={FreeFromIcon}></img><p></p>
                                          {new Intl.DateTimeFormat('en-GB', { 
                                                month: 'long', 
                                                day: '2-digit',
                                                year: 'numeric', 
                                                 hour: '2-digit',
                                                minute: '2-digit'
                                            }).format(new Date(announcement.freeFrom))}
                                        </td>
                                        <td data-label="Свободно до">
                                         <img   src={FreeToIcon}></img><p />
                                              {new Intl.DateTimeFormat('en-GB', { 
                                                month: 'long', 
                                                day: '2-digit',
                                                year: 'numeric', 
                                                 hour: '2-digit',
                                                minute: '2-digit'
                                            }).format(new Date(announcement.freeTo))}
                                        </td>
                                        <td>
                                            <IconButton>
                                                <img   src={BookImage} onClick={() => 
                                                    this.reserveAnnouncement(announcement.uid)}></img>
                                            </IconButton>
                                        </td>
                                        <td>
                                         {isAdmin && <IconButton>
                                                <DeleteIcon onClick={() => 
                                                    this.removeAnnouncement(announcement.uid)}>
                                                </DeleteIcon>
                                            </IconButton>}
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </div>
            </>
        )
    }
}

export default Announcement