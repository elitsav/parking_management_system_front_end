import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import axios from 'axios';


function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const styles = theme => ({
  paper: {
    // marginTop: theme.spacing(8),
    // marginTop:800,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    // margin: theme.spacing(1),
    // spacing:100,
    // backgroundColor: theme.palette.secondary.main,
    backgroundColor:'#dc004e',
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    // marginTop: theme.spacing(3),
    // marginTop:300,
  },
  submit: {
    marginTop:20
  },
  parkingSlot:{

  }
});

class SignUp extends React.Component{

  constructor() {
    super();
    this.state = {
      // firstName: "",
      // lastName: "",
      // email: "",
      // password: "",
      // passwordConfirm:"",
      // phoneNumber:"",
      // floor:"",
      // number:"",
      fields: {},
      errors: {}
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }
  handleChange = event=> {
    // this.setState({[event.target.name]:event.target.value, [event.target.name]:event.target.value,
    //   [event.target.name]:event.target.value,[event.target.name]:event.target.value,
    //   [event.target.name]:event.target.value,[event.target.name]:event.target.value,
    //   [event.target.name]:event.target.value,[event.target.name]:event.target.value,})
    let fields = this.state.fields;
    fields[event.target.name] = event.target.value;
    this.setState({
      fields
    });  }
  

  validateForm() {

    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

  
      if (!fields["email"].match("\\b[\\w.-]+@[-.\\w]+\\.[A-Za-z]{2,4}\\b")) {
        formIsValid = false;
        errors["email"] = "*Въведете валиден email адрес!";
      }
     
      if (!fields["phoneNumber"].match(/^[0-9]{10}$/)) {
        formIsValid = false;
        errors["phoneNumber"] = "*Въведете валиден телефонен номер!";
      }
      if (fields["floor"] > 3 ) {
        formIsValid = false;
        errors["floor"] = "*Невалидна стойност! Тази бизнес сграда има само 3 етажа!";
      }
      if (fields["number"] > 20 ) {
        formIsValid = false;
        errors["number"] = "*Невалидна стойност! Тази бизнес сграда има само 30 паркоместа!";
      }
      if(fields["password"] !== fields["passwordConfirm"]) {
            errors["passwordConfirm"] = 'Паролите не съвпадат !'
        }


    this.setState({
      errors: errors
    });
    return formIsValid;
  }
  
  handleFormSubmit = event => {
    
    event.preventDefault();

    const endpoint = "http://localhost:8090/user/register";

    if (this.validateForm()) {
      let fields = {};
      fields["firstName"] = this.state.firstName;
      fields["lastName"] = this.state.lastName;
      fields["email"] = this.state.email;
      fields["password"] = this.state.password;
      fields["passwordConfirm"] = this.state.passwordConfirm;
      fields["phoneNumber"] = this.state.phoneNumber;
      fields["floor"] = this.state.floor;
      fields["number"] = this.state.number;
      }

    const firstName = this.state.fields["firstName"];
    const lastName = this.state.fields["lastName"];
    const email = this.state.fields["email"];
    const password = this.state.fields["password"];
    const passwordConfirm = this.state.fields["passwordConfirm"];
    const phoneNumber = this.state.fields["phoneNumber"];
    const floor = this.state.fields["floor"];
    const number = this.state.fields["number"];

    const register_object = {
      firstName : firstName,
      lastName : lastName,
      email: email,
      password: password,
      passwordConfirm : passwordConfirm,
      phoneNumber : phoneNumber,
      "parkingSlot":{
      floor: floor,
      number: number,
      }
    };

    axios.post(endpoint, register_object).then(res => {
      console.log(res);
      console.log(res.data);
       this.props.history.push("/user/registration/confirm");
    }).catch(error => {
      this.setState({
          errorMsg : error.response.data.message
      })
  });
  ;
  
  };
  render(){
    const {classes} = this.props;
    const { autoClose, keepAfterRouteChange } = this.state;
            let { email } = this.state

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
        </Avatar>
        <Typography component="h1" variant="h5">
          Регистрация
        </Typography>
        {this.state.errorMsg && 
                    <div className="errorMsg">
                       {this.state.errorMsg}
                    </div>}
        <form className={classes.form} onSubmit={this.handleFormSubmit}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField initialValues={{email}}
              value={this.state.fields.firstName} onChange ={this.handleChange}
                autoComplete="fname"
                name="firstName"
                variant="outlined"
                required
                fullWidth
                id="firstName"
                label="Име"
                autoFocus
                enableReinitialize={true}
              />
             {this.state.errors.firstName &&  <div className="errorMsg">{this.state.errors.firstName}</div>}
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField value={this.state.fields.lastName} onChange ={this.handleChange}
                variant="outlined"
                required
                fullWidth
                id="lastName"
                label="Фамилия"
                name="lastName"
                autoComplete="lname"
              />
             {this.state.errors.lastName && <div className="errorMsg">{this.state.errors.lastName}</div>}
            </Grid>
            <Grid item xs={12}>
              <TextField value={this.state.fields.email} onChange ={this.handleChange}
                variant="outlined"
               required
                fullWidth
                id="email"
                label="Email адрес"
                name="email"
                autoComplete="email"
                enableReinitialize={true}
              />
            {this.state.errors.email && <div className="errorMsg">{this.state.errors.email}</div>}
            </Grid>
            <Grid item xs={12}>
              <TextField value={this.state.fields.password} onChange ={this.handleChange}
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Парола"
                type="password"
                id="password"
                autoComplete="current-password"
              />
              {this.state.errors.password && <div className="errorMsg">{this.state.errors.password}</div>}
            </Grid>
            <Grid item xs={12}>
              <TextField value={this.state.fields.passwordConfirm} onChange ={this.handleChange}
                variant="outlined"
                required
                fullWidth
                name="passwordConfirm"
                label="Потвърдете паролата"
                type="password"
                id="passwordConfirm"
                autoComplete="confirm-password"
              />
            {this.state.errors.passwordConfirm &&<div className="errorMsg">{this.state.errors.passwordConfirm}</div>}
            </Grid>
            <Grid item xs={12}>
              <TextField value={this.state.fields.phoneNumber} onChange ={this.handleChange}
                variant="outlined"
                required
                fullWidth
                name="phoneNumber"
                label="Телефонен номер"
                type="phoneNumber"
                id="phoneNumber"
                autoComplete="phoneNumber"
              />
            {this.state.errors.phoneNumber && <div className="errorMsg">{this.state.errors.phoneNumber}</div>}
            </Grid>
            <Grid item xs={12}>
            <Typography component="h3">
                Паркомясто:          
             </Typography>
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField value={this.state.fields.floor}onChange ={this.handleChange}
                autoComplete="floor"
                name="floor"
                variant="outlined"
                fullWidth
                id="floor"
                label="Етаж"
                autoFocus
              />
           {this.state.errors.floor && <div className="errorMsg">{this.state.errors.floor}</div>}
            </Grid>
            <Grid item xs={12} sm={6}>
              <TextField value={this.state.fields.number} onChange ={this.handleChange}
                variant="outlined"
                fullWidth
                id="number"
                label="Номер"
                name="number"
                autoComplete="number"
              />
             {this.state.errors.number && <div className="errorMsg">{this.state.errors.number}</div>}
            </Grid>     
          </Grid>
          <Button 
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          >
            Регистрация
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/" variant="body2">
                Вече имате акаунт? Влезте в системата!
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
  }
}
SignUp.propTypes = {
  classes: PropTypes.object.isRequired,
};
export default withStyles(styles)(SignUp);
