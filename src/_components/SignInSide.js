import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/styles';
import axios from 'axios';
import { alertService } from '../_services/alert.service';
import AuthenticationService from '../_services/AuthenticationService';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const styles = theme => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(https://images.unsplash.com/photo-1506521781263-d8422e82f27a?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1000&q=80)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:'#fafafa',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    spacing: [800,400],
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    marginTop:100,
    backgroundColor:'#f48fb1'
  },
  form: {
    width: '80%',
    marginTop:10,
  },
  submit: {
    spacing: [300, 0, 200],
  },
});

class SignInSide extends React.Component {
constructor() {
    super();
    this.state = {
      email: "",
      password: "",
    };
    this.handleChange = this.handleChange.bind(this);
    this.handleFormSubmit = this.handleFormSubmit.bind(this);
  }
  handleChange = event=> {
    this.setState({[event.target.name]:event.target.value, [event.target.name]:event.target.value})
  }

  handleFormSubmit = event => {
    event.preventDefault();

    const endpoint = "http://localhost:8090/user/login";

    const email = this.state.email;
    const password = this.state.password;

    const user_object = {
      email: email,
      password: password
    };

    axios.post(endpoint, user_object).then(res => {
      AuthenticationService.successfulLoginVerification(res.data.jwt, res.data.email, res.data.roleName)
    //  return this.handleDashboard();
    this.props.history.push("/announcements");
      console.log(res);
      console.log(res.data)
    }).catch(error => {
      this.setState({
          errorMsg : error.response.data.message
      })
  });
  
  };
  // handleDashboard() {
  //   axios.get("http://localhost:8090/dashboard").then(res => {
  //     if (res.data === "success") {
  //       this.props.history.push("/dashboard");
  //     } else {
  //       alert("Authentication failure");
  //     }
  //   });
  // }


  render() {
      const {classes} = this.props;
     const {autoClose, keepAfterRouteChange} = this.state;
  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
         <Typography component="h1" variant="h4">
            <center><mark><i><b>PARKING MANAGEMENT SYSTEM</b></i></mark></center>       
        </Typography>
          <Avatar className={classes.avatar}>
          </Avatar>
          <Typography component="h1" variant="h5">
            Вход в системата :
          </Typography>
          <form className={classes.form} onSubmit={this.handleFormSubmit}>
          
            <TextField onChange ={this.handleChange}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email адрес"
              name="email"
              autoComplete="email"
              autoFocus
            />                   
            <TextField onChange ={this.handleChange}
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Парола"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Вход
            </Button>
            {this.state.errorMsg && 
                    <div className="eli">
                      <i class="fa fa-exclamation-circle"></i>
                        <strong>{this.state.errorMsg}</strong>
                    </div>}
            <Grid container>
              <Grid item xs>
                <Link href="/resetPassword" variant="body2">
                  Забравена парола?
                </Link>
                <Link href="/user/registration/confirm" variant="body2">
                <p />
                  Потвърдедете регистрацията си!
                </Link>
              </Grid>
              <Grid item>
                <Link href="/register" variant="body2">
                  {"Нямате акаунт? Регистрирайте се!"}
                </Link>
              </Grid>
              <Grid item >
                
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
  }
}
SignInSide.propTypes = {
    classes: PropTypes.object.isRequired,
  };

export default withStyles(styles)(SignInSide);

