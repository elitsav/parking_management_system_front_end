import React, {Component} from 'react'

import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import CalendarTodayIcon from '@material-ui/icons/CalendarToday';
import ContactMailIcon from '@material-ui/icons/ContactMail';
import LocalParkingIcon from '@material-ui/icons/LocalParking';
import BusinessIcon from '@material-ui/icons/Business';
import UserService from '../_services/UserService.js'
import AuthenticationService from '../_services/AuthenticationService.js'
import BookImage from '../images/booking_btn.png'
import CreatedAtIcon from '../images/calendar.png'
import FreeFromIcon from '../images/free_from.png'
import FreeToIcon from '../images/free_to.png'
import ActivateUserIcon from '../images/confirmed.png'
import ParkingFloorIcon from '../images/parking_floor.png'
import ParkingSlotIcon from '../images/parking_slot.png'
import UserNameIcon from '../images/name.png'
import MailIcon from '../images/mail.png'
import PhoneIcon from '../images/phone.png'
import Layout from './Layout';
import Header from './Header/HeaderComponent.js'
import LoadingIndicator from './LoadingIndicator'
import './Announcement/Announcement.css'
import { Link } from 'react-router-dom';
import axios from "axios";


class DeactivatedUsers extends Component {

    constructor(props) {
        super(props)

        this.state = {
            successMsg : '',
            errorMsg : '',
            isLoading : false,
            users : []
        }
        
        this.getAllDeactivatedUsers = this.getAllDeactivatedUsers.bind(this)
        this.activateUser=this.activateUser.bind(this)
    }

    componentDidMount() {
        this.getAllDeactivatedUsers()
         const { match: { params } } = this.props;

  axios.get(`http://localhost:8090/user/profile/${params.userUid}`)
    .then(({ data: user }) => {
      console.log('user', user);

      this.setState({ user });
    });
    }

    getAllDeactivatedUsers() {
       AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });

        UserService.getAllDeactivatedUsers()
        .then(response => {
            this.setState({
                isLoading : false,
                users : response.data
            })
        })
    }

   activateUser(userUid) {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });
    
        UserService.activateUser(userUid)
        .then(response => {
            this.setState({
                isLoading : false,
                successMsg : 'Успешно активиран потребител!'
            })
            this.getAllDeactivatedUsers()
        })
        .catch(() => {
            this.setState({
                errorMsg : `Възникна грешка. Моля опитайте по-късно !`
            })
        })
    }

    render() {
        if(this.state.isLoading) {
           return <LoadingIndicator/>
        }
        return (
            <>
            <Header></Header>
            <Layout />
            <div className="announcements">
                <div className="bs-example">
                    {this.state.successMsg !== '' && 
                    <div className="alert alert-success alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.successMsg}</strong>
                    </div>}
                    {this.state.errorMsg !== '' && 
                    <div className="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.errorMsg}</strong>
                    </div>} 
                    <h1>ДЕАКТИВИРАНИ ПОТРЕБИТЕЛИ</h1>
                    <table className="table table-hover">
                        <thead>
                                <th width='20%'>Email адрес<p /> <img src={MailIcon}></img></th>
                                <th width='20%'>Име<p /><img src={UserNameIcon}></img></th>
                                <th width='20%'>Телефонен номер<p /><img src={PhoneIcon}></img></th>
                                <th  width='15%'><mark>Активирай потребител</mark></th>
                        </thead>
                        <tbody>
                        {
                            this.state.users.map(
                                user => 
                                  <tr key={user.uid}>
                                    <td data-label="Email адрес">
                                      <Link to={`/user/details/${user.uid}`}>
                                       {user.email} </Link></td>
                                    <td data-label="Име">
                                       {user.firstName}{' '}{user.lastName} 
                                    </td>
                                     <td data-label="Телефонен номер">
                                       {user.phoneNumber}
                                    </td>                               
                                     <td>
                                         <IconButton>
                                           <img   src={ActivateUserIcon} onClick={() => 
                                              this.activateUser(user.uid)}></img>
                                            </IconButton>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </div>
            </>
        )
    }
}

export default DeactivatedUsers;