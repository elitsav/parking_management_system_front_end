import axios from "axios";

class ParkingSlotService {

  removeParkingSlot(parkingSlotUid) {
    return axios.delete(
      "http://localhost:8090/parkingSlot/remove?parkingSlotUid=" +
        `${parkingSlotUid}`
    );
  }
}

export default new ParkingSlotService();
