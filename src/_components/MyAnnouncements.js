import React, {Component} from 'react'

import { IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EmailIcon from '@material-ui/icons/Email';
import AnnouncementService from '../_services/AnnouncementService.js'
import AuthenticationService from '../_services/AuthenticationService.js'
import Layout from './Layout'
import LoadingIndicator from './LoadingIndicator'
import AnnouncementImage from '../images/announcement.jpg'
import CreatedAtIcon from '../images/calendar.png'
import FreeFromIcon from '../images/free_from.png'
import FreeToIcon from '../images/free_to.png'
import Header from "./Header/HeaderComponent.js";

class MyAnnouncements extends Component {

    constructor(props) {
        super(props)

        this.state = {
            successMsg : '',
            errorMsg : '',
            isLoading : false,
            announcements : []
        }
        
        this.getAllAnnouncementsCreatedByUser = this.getAllAnnouncementsCreatedByUser.bind(this);
        this.removeAnnouncement=this.removeAnnouncement.bind(this)
    }

    componentDidMount() {
        this.getAllAnnouncementsCreatedByUser()
    }

    getAllAnnouncementsCreatedByUser() {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });

        AnnouncementService.getAllAnnouncementsCreatedByUser()
        .then(response => {
            this.setState({
                isLoading : false,
                announcements : response.data
            })
        })
    }
    removeAnnouncement(announcementUid) {
        AuthenticationService.setupAxiosInterceptors()
        this.setState({
            isLoading : true
        });

        AnnouncementService.removeAnnouncement(announcementUid)
         .then(response => {
            this.setState({
                isLoading : false,
                successMsg : "Обявата е успешно премахната!"
            })
            this.getAllAnnouncementsCreatedByUser()
            
        })
        .catch(() => {
            this.setState({
                errorMsg : `Възникна грешка! Моля опитайте по-късно ! !`
            })
        })
    }
    

    render() {
        if(this.state.isLoading) {
            return <LoadingIndicator/>
        }
        return (
            <>
            <Header></Header>
            <Layout></Layout>
            <div className="announcements">
                <div className="bs-example">
                    {/* <center><img className="announcementImage" alt="" src={AnnouncementImage}></img></center> */}
                     <center> <h1>МОИТЕ ОБЯВИ </h1></center>
                    {this.state.successMsg !== '' && 
                    <div className="alert alert-success alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.successMsg}</strong>
                    </div>}
                    {this.state.errorMsg !== '' && 
                    <div className="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" className="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <strong>{this.state.errorMsg}</strong>
                    </div>} 
                    <table className="table table-hover">
                        <thead>
                            {/* <tr> */}
                                <th width='10%'>Дата на създаване</th>
                                <th width='10%'>Свободно от</th>
                                <th width='10%'>Свободно до</th>
                                <th width='10%'>Статус</th>
                                <th width='5%'>Изтрий</th>
                            {/* </tr> */}
                        </thead>
                        <tbody>
                        {
                            this.state.announcements.map(
                                announcement => 
                                    <tr key={announcement.uid}>
                                         <td data-label="Дата на създаване" >
                                        <img   src={CreatedAtIcon}></img>
                                        <p />
                                                {new Intl.DateTimeFormat('en-GB', { 
                                                month: 'long', 
                                                day: '2-digit',
                                                year: 'numeric', 
                                                 hour: '2-digit',
                                                minute: '2-digit'
                                            }).format(new Date(announcement.creationTime))}
                                        </td>
                                         <td data-label="Свободно от">
                                        <img   src={FreeFromIcon}></img><p></p>
                                          {new Intl.DateTimeFormat('en-GB', { 
                                                month: 'long', 
                                                day: '2-digit',
                                                year: 'numeric', 
                                                 hour: '2-digit',
                                                minute: '2-digit'
                                            }).format(new Date(announcement.freeFrom))}
                                        </td>
                                        <td data-label="Свободно до">
                                         <img   src={FreeToIcon}></img><p />
                                              {new Intl.DateTimeFormat('en-GB', { 
                                                month: 'long', 
                                                day: '2-digit',
                                                year: 'numeric', 
                                                 hour: '2-digit',
                                                minute: '2-digit'
                                            }).format(new Date(announcement.freeTo))}
                                        </td>
                                        <td data-label="Статус">
                                         <img   src={FreeToIcon}></img><p />
                                            {announcement.announcementStatus}
                                        </td>
                                        <td>
                                            <IconButton>
                                                <DeleteIcon onClick={() => 
                                                    this.removeAnnouncement(announcement.uid)}>
                                                </DeleteIcon>
                                            </IconButton>
                                        </td>
                                    </tr>
                            )
                        }
                        </tbody>
                    </table>
                </div>
            </div>
            </>
        )
    }
}

export default MyAnnouncements